#include <stdio.h>
#define    N    30
void findMax(int score[], int num[], int n, int *pMaxScore, int *pMaxScoreId)
{
	int m=0;
	*pMaxScore=score[0];
	for(int i=0;i<n;i++)
	{		if(*pMaxScore<score[i])
		{
			int t;
			t=*pMaxScore;
			*pMaxScore=score[i];
			score[i]=t;
			m=i;
		}
	}
	*pMaxScoreId=*(pMaxScoreId+m+1);
}

int main() 
{
	int i, n;
	int scores[N];
	int ids[N];
	int maxScore, maxScoreId;
	printf("input students count:\n");
	scanf("%d", &n);
	for (i = 0; i < n; i++) 
	{
		printf("(%d): score and id\n", i + 1);
		scanf("%d %d", &scores[i], &ids[i]);
	}
	findMax(scores, ids, n, &maxScore, &maxScoreId);
	printf("max score: %d -- %d\n", maxScore, maxScoreId);
	return 0;
}
