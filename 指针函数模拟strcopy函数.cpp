#include <stdio.h>
void strcopy(char *dest, char *src)
{
	while(*src!='\0')
	{
		*dest++=*src++;
	}
	*dest=*src;
}
int main() 
{
	char buf[64];
	char *s = "ABCDEFGH";
	strcopy(buf, s);
	printf("%s\n", buf);
	return 0;
}
