#include"stack.h"
//创造节点
SL* creat(DataType x)
{
	SL* node = (SL*)malloc(sizeof(SL));
	if (node ==  NULL)
	{
		perror("malloc fail!");
		exit(1);
	}
	node->data = x;
	node->next = NULL;
	return node;
}
//进行初始化
void SLinit(SLF* e)
{
	assert(e);
	e->count = 0;;
	e->top = NULL;
}
//判断栈空
bool SLempty(SLF* e)
{
	return e->count ? false : true;
}
//入栈
void SLpush(SLF* e,DataType x)
{
	//创造节点
	SL* cur = creat(x);
	if (e->count == 0)
	{
		e->top = cur;
		e->count++;
	}
	else
	{
		cur->next = e->top;
		e->top = cur;//重新指向栈顶元素
		e->count++;
	}
}
//取出栈顶元素
DataType SLtop(SLF* e)
{
	return e->top->data;//直接返回元素
}
//删除栈顶元素
void SLpop(SLF* e)
{
	SL* delete = e->top;
	e->top = e->top->next;
	free(delete);
	e->count--;
}
//销毁栈
void SLdestory(SLF* e)
{
	while (e->count != 0)
	{
		SL* pre = e->top;
		e->top = pre->next;
		free(pre);
		e->count--;
	}
	return;
}
//获取栈的元素个数
int SLsize(SLF* e)
{
	return e->count;
}