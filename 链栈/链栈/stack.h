#pragma once
#include<stdio.h>
#include<assert.h>
#include<stdbool.h>
#include<stdlib.h>
typedef int DataType;
typedef struct stack
{
	DataType data;
	struct stack* next;
}SL;
typedef struct SLF//用来统计栈元素和插入和删除操作
{
	SL* top;
	int count;
}SLF;

//函数声明
//1.创造节点
SL* creat(DataType x);
//2.进行初始化
void SLinit(SLF* e);
//判断栈空
bool SLempty(SLF* e);
//入栈
void SLpush(SLF* e, DataType x);
//取出栈顶元素
DataType SLtop(SLF* e);
//删除栈顶元素
void SLpop(SLF* e);
//销毁栈
void SLdestory(SLF* e);
//获取栈的元素个数
int SLsize(SLF* e);




