#define  _CRT_SECURE_NO_WARNINGS 1      
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// 定义资产结构体
typedef struct property {
    int id;
    char name[50];
    float value;
    char description[100];
}pro;

typedef struct a
{
	pro a[100];
	int size;//数组中的元素个数 
}sl;


// 录入资产信息函数
//添加 
void add(sl* s) {
    printf("请输入资产编号: ");
    scanf("%d", &s->a[s->size].id);
    printf("请输入资产名称: ");
    scanf("%s", s->a[s->size].name);
    printf("请输入资产价值: ");
    scanf("%f", &(s->a[s->size].value));
    printf("请输入资产描述: ");
    scanf("%s", s->a[s->size].description);
    s->size++;
}

// 展示资产信息函数
void show(sl* s) {
	printf("编号 名称 价值  描述\n");
	for(int i=0;i<s->size;i++)
	{
		printf("%d %s %.2f %s\n",s->a[i].id,s->a[i].name,s->a[i].value,s->a[i].description);
	}
	printf("\n\n");
}


// 查询资产信息函数
//按照编号查找 
void find_id(sl* s,int id) {
    for (int i = 0; i < s->size; i++) {
        if (s->a[i].id == id) {
        	printf("编号 名称 价值  描述\n");
           	printf("%d %s %.2f %s\n",s->a[i].id,s->a[i].name,s->a[i].value,s->a[i].description);
            return ;
        }
    }
        printf("未找到对应资产!\n");
}
//删除指定资产 
void pop(sl* s)
{
	int id;
	printf("请输入要删除的资产的编号>\n");
	scanf("%d",&id);
	int pos;
	//查找要删除的id 
	int flag=0;//标记 
	for(int i=0;i<s->size;i++)
	{
		if(s->a[i].id==id)
		{
			flag=1;
			pos=i;
			break;
		}
	}
	if(flag==0)
	{
		printf("不存在该资产\n");
		return ;
	}
	for(int i=pos+1;i<s->size;i++)
	{
		s->a[i-1]=s->a[i];
	}
	s->size--;
	printf("删除成功\n\n");
}

//修改
void fix(sl* s)
{
	printf("请输入要修改的资产的编号>\n");
	int id;
	scanf("%d",&id);
	int pos;
	//查找要修改的id 
	int flag=0;//标记 
	for(int i=0;i<s->size;i++)
	{
		if(s->a[i].id==id)
		{
			flag=1;
			pos=i;
			break;
		}
	}
	if(flag==0)
	{
		printf("不存在该资产\n");
		return ;
	}
	
	printf("请输入资产编号: ");
    scanf("%d", &s->a[pos].id);
    printf("请输入资产名称: ");
    scanf("%s", s->a[pos].name);
    printf("请输入资产价值: ");
    scanf("%f", &(s->a[pos].value));
    printf("请输入资产描述: ");
    scanf("%s", s->a[pos].description);
    printf("修改成功\n");
 } 
 
 void sum(sl* s)
 {
 	float k=0;
 	for(int i=0;i<s->size;i++)
 	k+=s->a[i].value;
 	printf("总资产为:%.2f\n",k);
 }
 

int main() {
	
	sl s;
	s.size=0;//初始化 
	char key[20]="666";//密码 
	char key1[20];
    printf("欢迎使用资产信息管理系统!\n");
    printf("请输入密码\n");
    scanf("%s",key1);
    if(strcmp(key,key1)!=0)
    {
    	printf("密码错误\n");
    	return -1;
	}
	printf("登录成功\n\n");
	int choice=0;
    do{
        printf("1. 录入资产\n2. 查询资产\n3.查看\n4.删除\n5.修改\n6.计算总资产\n0. 退出\n");
        printf("请选择> \n");
        scanf("%d", &choice);
        switch (choice) {
        case 1:
		printf("录入资产>\n");     
            add(&s);
            break;
        case 2:
            printf("请输入要查询的资产编号> \n");
            int id;
            scanf("%d", &id);
            find_id(&s,id);
            break;
        case 3:
		      show(&s);
		      break;
		case 4:
			pop(&s);
			break;
		case 5:
			fix(&s);
			break;
		case 6:
			sum(&s);
			break;
       case 0:
            printf("退出系统......\n");
            break;
        default:
            printf("无效选择，请重新输入!\n");
        }
    }while(choice);

    return 0;
}
