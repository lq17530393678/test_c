#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= n; j++)
		{
			printf("%dx%d=%-d ", j, i, i * j);
			if (i == j)
			{
				break;
			}
		}
		printf("\n");
	}
}
