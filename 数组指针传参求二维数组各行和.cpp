//求二维数组各行的和 
#include<stdio.h>
void sum(int (*p)[5],int x,int y)
{
	for(int i=0;i<x;i++)
	{
		int sum=0;
		for(int j=0;j<y;j++)
		{
			sum+=*((*p+i)+j);
		}
		printf("sum=%d\n",sum);
	}
}
int main()
{
	int arr[3][5]={{1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7}};
	sum(arr,3,5);
	return 0;
 } 
