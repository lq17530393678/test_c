#include<stdio.h>
void fun (char *p)//形参开辟了一块与str一样的空间; 
{
	*p=(char *)malloc(100);//对p进行动态内存开辟，
	//此时仅仅只是p指向了这一块空间， 
	//函数结束,p自动释放，但是堆上的空间没有释放，造成内存泄漏 
}
int main()
{
	char *str==NULL;//str指针为空指针，并没有指向任何有效空间； 
	fun();//在函数中对形参分配了一份动态内存 
	strcpy(str,"hello world");//此时str仍然是空指针；形参指针的开辟与此空间不同， 
	return 0;//无法拷贝; 
}
