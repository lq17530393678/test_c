#include<stdio.h>
#include<stdlib.h>
typedef struct ps
{
	int data;
	struct ps* next;
}SL;
int main()
{
	SL* phead=(SL*)malloc(sizeof(SL));
	SL* s1=(SL*)malloc(sizeof(SL));
	SL* s2=(SL*)malloc(sizeof(SL));
	phead->next=s1;
	s1->next=s2;
	s2->next=NULL;
	SL* pcur=phead;
	int i=0;
	while(pcur)
	{
		pcur->data=i++;
		pcur=pcur->next;
	}
	SL* ptail=phead;
	while(ptail)
	{
		printf("%d ",ptail->data);
		ptail=ptail->next;
	}
	return 0;
}
