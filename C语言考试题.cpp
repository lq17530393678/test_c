#include<stdio.h> 
void fun(int arr[],int n,int *min,int  *max)
{
	 *min=arr[1];
	 *max=arr[0];
	for(int i=2;i<n;i=i+2)
	{
		if(arr[i]>*max)
		{
			*max=arr[i];//奇数项的最大值 
		}
	}
	for(int j=3;j<n;j++)
	{
		if(arr[j]<*min)
		{
			*min=arr[j];//偶数项的最大值 
		}
	}
}
int main()
{
	int n=0;
	scanf("%d",&n);
	int arr[n];
	for(int i=0;i<n;i++)
	{
		scanf("%d",&arr[i]);
	}
	int min,max;
	fun(arr,n,&min,&max);
	printf("%d %d",min,max);
	return 0;
}
