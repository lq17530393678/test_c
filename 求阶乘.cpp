#include<iostream>
using namespace std;
int fact1(int n)
{
	int sum=1;
	for(int i=1;i<=n;i++)
	{
		sum*=i;
	}
	return sum;
}
int fact2(int n)
{
	if(n==1)
	return 1;
	else
	return n*fact2(n-1);
}
int main()
{
	int n=0;
	cin>>n;
	cout<<fact1(n)<<endl;
	cout<<fact2(n)<<endl;
	return 0;
}
