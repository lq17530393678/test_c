#include<stdio.h>
#pragma pack(1)//取消默认对齐数 
struct A
{
	char a;
	int b;
	char c;
};
#pragma pack()//恢复原来的对齐数 
int main()
{
	printf("%d ",sizeof(struct A));//计算结构体的大小 
	return 0;
}
