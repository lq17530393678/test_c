#include<stdio.h>
int main()
{//int 的最大值很大
	//char (有符号)的值域是-128-127;超出就会对应返回；
	//unsigned char 的值域是0-255；肯定是正整数；超出的对应返回；
	unsigned char a;
	a = 257;
	printf("%u,%d\n", a,a);//用有符号的形式来输出无符号正数，那么%u %d,没有区别
	char b = -129;
	printf("%d\n", b);
	char c = 128;
	printf("%d\n", c);
	unsigned char d = -1;
	printf("%u %d\n", d,d);
	unsigned  int i = -1;//可以用来求无符号整形的最大值
	printf("%u,%d\n", i, i);//如果将无符号行的数以整形的形式输出,则输出时就会认定是有符号型的
	int k = -2147483649;                               
	printf("%d", k);
	//当值超出所对类型的范围时，就会重新回到另一个最值，再次进入值域。
}//如果无符号数是负数的话用%u来输出就会输出对应的接近最大值的数(倒着数)；