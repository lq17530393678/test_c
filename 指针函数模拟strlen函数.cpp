#include <stdio.h>
int strlength(char *s) 
{
	int count=0;
	while(*s!='\0')
	{
		s++;
		count++;
	}
	return count;
}
int main() 
{
	char s[] = { "ABCDEFGH" };
	printf("%s: %d\n", s, strlength(s));
	return 0;
}
