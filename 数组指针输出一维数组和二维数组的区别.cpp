#include<stdio.h>
//数组指针输出一维数组和二维数组的区别 
int main()
{
	int arr[2][3]={{1,2,3},{4,5,6}};
	int (*p)[3]=arr;//数组指针指向的是一整个数组，二维数组数组名就是第一行的一维数组所以不用加&
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<3;j++)
		{
			//printf("%d ",*(*(p+i)+j));
		      printf("%d ",(*(p+i))[j]);//第二种输出方式 
		}
		printf("\n");
	 }
	 int arr2[6]={1,2,3,4,5,6};
	 int (*pa)[6]=&arr2;
	 for(int i=0;i<6;i++)
	 {
	 	//printf("%d ",(*pa)[i]);
	 	printf("%d ",*(*p+i)); //两种输出形式 
	 }
 } 
