
#include <stdio.h>
void removeSpace(char *s)
{
	char* ptr = s;
    while (*s) {
        if (*s != ' ')
		{
            *ptr++ = *s;
        }
        s++;
    }
    *ptr = '\0';
}
int main()
{
    char s[81];
    gets(s);
    removeSpace(s);
    printf("%s", s);
    return 0;
}
