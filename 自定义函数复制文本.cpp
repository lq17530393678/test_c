#include<stdio.h>
int fuzhi(char *name1,char *name2)
{ 
	FILE *pf1=fopen(name1,"r");
	if(pf1==NULL)
	{
		printf("打开源文件失败\n");
		return 0;
	}
	FILE *pf2=fopen(name2,"w");
	if(pf2==NULL)
	{
		printf("打开文件失败\n");
		fclose(pf1);
		return 0;
	}
	int ret=fgetc(pf1);
	while(ret!=EOF)
	{
		fputc(ret,pf2);
		ret=fgetc(pf1);
	}
	fclose(pf1);
	fclose(pf2);
	pf1=NULL;
	pf2=NULL;
	return 1;
}

int main()
{
	char ch1[]="测试1.txt";
	char ch2[]="测试2.txt";
	if(fuzhi(ch1,ch2)==1)
	{
		printf("复制成功\n");
	}
	return 0;
}
