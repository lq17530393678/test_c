//函数指针2加3
#include<stdio.h>
//先定义一个求和函数 
int sum(int a,int b)
{
	return a+b;
} 
int main()
{
    //int (*p)(int ,int )=sum;//输入的2种方式 
    int (*p)(int ,int )=&sum;
    int x,y;
    scanf("%d%d",&x,&y);
    printf("%d\n",p(x,y));//输出的3种方式 
    printf("%d\n",(*p)(x,y));
    printf("%d",sum(x,y));
    //总结:函数指针的*无用,可不加; 
} 
