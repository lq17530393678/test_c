#include<stdio.h>
#include<stdlib.h>
int main()
{
	int *p=(int *)malloc(sizeof(int)*10);//分配10个整形空间 
	if(p==NULL)
	{
		perror("malloc!");
	}
	for(int i=0;i<10;i++)
	{
		printf("%d ",*p++);//里面是随机值 
	}
	free(p);//进行内存释放 
	p=NULL;
	return 0;
}
