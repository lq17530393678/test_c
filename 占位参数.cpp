////定义结构体
////1.函数默认值的使用
//int  fun(int a, int b=20, int c=30)//函数的默认值
//{
//	return a + b + c;
//}
////2.如果中间的形参有默认值那么后面的形参必须也得有默认值
//int fun2(int a, int b = 6, int c=5, int d=4)
//{
//	return a + b + c + d;
//}
////否则在输入形参时,有默认值的位置必须输入
////3.函数的声明中如果形参有默认值,函数的定义不能有默认值(不能重复)
//int fun3(int a, int b = 5);
//
//int fun3(int a = 4, int b)//没有默认值的可以给默认值
//{
//	return a + b;
//}
////4.占位参数
//void fun4(int, int)
//{
//	cout << "nihao" << endl;
//}
//int main()
//{
//	cout << fun(10) << endl;//必须保证形参有值
//	cout << fun(50,10) << endl;//再次传参就会覆盖默认值
//	cout << fun2(1) << endl;//默认值后面必须全是默认值
//	cout << fun3()<<endl;
//	 fun4(1,1);
//
//	return 0;
//}
//
//
