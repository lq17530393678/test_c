#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
int main()
{
	int* p = (int*)calloc(10,sizeof(int));
	if (p == NULL)
	{
		perror("malloc fail!");
		return 1;
	}
	int* ptr;
	ptr = (int*)realloc(p, 20* sizeof(int));
	if (ptr== NULL)
	{
		perror("realloc fail!");
		return 1;
	}
	p = ptr;
	free(ptr);
	free(p);
	ptr = NULL;
	p = NULL;
	return 0;
}
