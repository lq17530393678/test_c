#include<stdio.h>
#define M 3
#define N 2
struct student 
{
	int xh;
	char name[99];
	float course[N];
	float ave;
};
void input(struct student arr[])
{
	for(int i=0;i<M;i++)
	{	
			scanf("%d%s%f%f",&arr[i].xh,arr[i].name,&arr[i].course[0],&arr[i].course[1]);
			arr[i].ave=(arr[i].course[0]+arr[i].course[1])/2;
	}
}
void output(struct student arr[])
{
	printf("input:\nStudent information:\n");
	for(int i=0;i<M;i++)
	{
		printf("%d,%s,%.0f,%.0f,%.2f\n",arr[i].xh,arr[i].name,arr[i].course[0],arr[i].course[1],arr[i].ave);
	}
	struct student t=arr[0];
	for(int i=0;i<M;i++)
	{
		if(arr[i].ave>t.ave)
		t=arr[i];
	}
	printf("Max information:\n");
	printf("%d,%s,%.0f,%.0f,%.2f",t.xh,t.name,t.course[0],t.course[1],t.ave);
}

int main()
{
	struct student arr[M];
	input(arr);
	output(arr);
}
