#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
//作业内容
#define INIT_CAPACITY 4
typedef int SLDataType;
// 动态顺序表 -- 按需申请
typedef struct SeqList
{
    SLDataType* a;
    int size;     // 有效数据个数
    int capacity; // 空间容量
}SL;


void SLInit(SL* ps)
{
    ps->size = 0;
    ps->capacity = INIT_CAPACITY;
    ps->a = NULL;
}


void SLDestroy(SL* ps)
{
    ps->a = NULL;
    ps->capacity = 0;
    ps->size = 0;
}

void SLPrint(SL* ps)
{
    for (int i = 0; i < ps->size; i++)
    {
        printf("%d ", ps->a[i]);
    }
    printf("\n");
}
//初始化和销毁
void SLInit(SL* ps);
void SLDestroy(SL* ps);
void SLPrint(SL* ps);

//扩容
void SLCheckCapacity(SL* ps)
{
    if (ps->capacity == ps->size)
    {
        int newcapacity = (ps->capacity == 0) ? 4 : 2 * ps->capacity;
        SLDataType* p = (int*)realloc(ps->a, sizeof(SLDataType) * newcapacity);
        if (p == NULL)
        {
            perror("realloc fail!\n");
        }
        ps->a = p;
        ps->capacity = newcapacity;
    }
}
void SLCheckCapacity(SL* ps);


//头插
void SLPushFront(SL* ps, SLDataType x)
{
    assert(ps);
    assert(ps->capacity);
    SLCheckCapacity(ps);
    for (int i = 0; i < ps->size; i++)
    {
        ps->a[i + 1] = ps->a[i];
    }
    ps->a[0] = x;
    ps->size++;
}
//头删
void SLPopFront(SL* ps)
{
    assert(ps);
    assert(ps->size);
    for (int i = 1; i < ps->size; i++)
    {
        ps->a[i - 1] = ps->a[i];
    }
    ps->size--;
}
//尾删
void SLPopBack(SL* ps)
{
    assert(ps);
    assert(ps->size);
    ps->size--;
}

//尾插
void SLPushBack(SL* ps, SLDataType x)
{
    SLCheckCapacity(ps);
    ps->a[ps->size] = x;
    ps->size++;
}


//在指定位置插入
void SLInsert(SL* ps,int pos,SLDataType x)
{
    assert(ps);
    assert(pos-1 >= 0 && pos-1 <= ps->size);
    SLCheckCapacity(ps);
    for (int i = ps->size - 1; i >= pos - 1; i--)
    {
        ps->a[i + 1] = ps->a[i];
    }
    ps->a[pos - 1] = x;
    ps->size++;
}
//在指定位置删除
void SLErase(SL* ps, int pos)
{
    assert(ps);
    assert(pos-1 >= 0 && pos-1 <= ps->size);
    for (int i = pos; i < ps->size; i++)
    {
        ps->a[i - 1] = ps->a[i];
    }
    ps->size--;
}

//查找数据
int SLFind(SL* ps, SLDataType x)
{
    int i = 0;
    for ( i = 0; i < ps->size; i++)
    {
        if (ps->a[i] == x)
        {
            return i;
        }
    }
    return 0;
}

//指定位置之前插入/删除数据
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);
int SLFind(SL* ps, SLDataType x);



//头部插入删除 / 尾部插入删除
void SLPushBack(SL* ps, SLDataType x);
void SLPopBack(SL* ps);
void SLPushFront(SL* ps, SLDataType x);
void SLPopFront(SL* ps);

