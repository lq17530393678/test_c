#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//按位操作符
//  <<  >>  &  ^  |  ~  6个
//只有~是单目运算符
int mount(int num)
{
	int count = 0;
	for (int i = 0; i < 32; i++)
	{
		if ((num >> i) & 1 == 1)
			count++;
	}
	return count;
}
int main()
{
	////求一个非负数二进制数的1的个数
	//int a = 15;
	//int mine = 1;
	//int count = 0;
	//for (int i = 0; i < 32; i++)
	//{
	//	if (a & mine == 1)
	//		count++;
	//	a >>= 1;
	//}
	//printf("%d\n", count);
	int num = 0;
	scanf("%d", &num);
	int count = mount(num);
	printf("%d", count);
}
