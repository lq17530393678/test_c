

#include"good.h"

//初始化
void init(pro* ps)
{
	assert(ps);    
	ps->arr = NULL;
	ps->size = ps->capacity = 0;
}

//销毁
void destory(pro* ps)
{
	assert(ps);
	if (ps->arr) 
	{ 
		free(ps->arr); 
		ps->arr = NULL;  
	}  
	ps->capacity = ps->size = 0;
}


//扩容
void check(pro* ps)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		//记录的总空间翻倍
		int newcapacity = (ps->capacity == 0) ? 4 : 2 * ps->capacity;
		//顺序表的空间也翻倍
		good* newarr = (good*)realloc(ps->arr,sizeof(good) * newcapacity); 
		//  判断是否扩容成功		
		if (newarr == NULL)
		{
			perror("realloc fail!\n");
			exit(1);
		}
		//将新的空间给到数组空间 
		ps->arr = newarr;
		//将新的空间大小重新赋值
		ps->capacity = newcapacity;
	}
}
//添加
void add(pro* ps)
{
	assert(ps);
	good tmp; 
	printf("请输入商品的编号>\n");
	scanf("%s", tmp.id); 
	printf("请输入商品的名字>\n");
	scanf("%s", tmp.name); 
	printf("请输入商品的进价>\n");
	scanf("%lf", &tmp.price_in); 
	printf("请输入商品的售价>\n");
	scanf("%lf", &tmp.price_out); 
	printf("请输入商品的进货量>\n"); 
	scanf("%d", &tmp.count_in);  
	printf("请输入商品的售货量>\n");  
	scanf("%d", &tmp.count_out);  
	check(ps);
	ps->arr[ps->size++] = tmp; 
	printf("添加成功!\n\n");
}


//查找名字
int find_name(pro* ps)
{
	assert(ps);
	char tmp_name[max_name];
	scanf("%s", tmp_name);
	for (int i = 0; i < ps->size; i++)
	{
		if (strcmp(ps->arr[i].name, tmp_name) == 0) 
		{
			return i;
		}
	}
	return -1;
}
//查找编号
int find_id(pro* ps)
{
	assert(ps);
	char tmp_id[max_id];
	scanf("%s", tmp_id);   
	for (int i = 0; i < ps->size; i++) 
	{ 
		if (strcmp(ps->arr[i].id, tmp_id) == 0 ){ 
			return i; 
		}
	}
	return -1;
}

//指定该位置删除
void pop_pos(pro* ps, int pos)
{
	assert(ps);
	for (int i = pos + 1; i < ps->size; i++)
	{
		ps->arr[i - 1] = ps->arr[i]; 
	}
	ps->size--;
} 
//删除
void pop(pro* ps)
{
	assert(ps);
	printf("1.通过编号删除\n");
	printf("2.通过名字删除\n"); 
	printf("请选择>\n"); 
	int op=0;
	scanf("%d", &op); 
	switch (op)
	{
	case 2: {
		printf("请输入要删除商品的名字>\n");
		int pos = find_name(ps);
		if (pos < 0)
		{
			printf("没有该商品的信息\n\n");
			break;
		}
		//删除该位置的信息
		pop_pos(ps, pos);
		printf("删除成功!\n\n");
		break;
		}
	case 1: {
		printf("请输入要删除商品的编号>\n"); 
		int pos = find_id(ps);  
		if (pos < 0) 
		{
			printf("没有该商品的信息\n\n");
			break;
		}
		//删除该位置的信息
		pop_pos(ps, pos); 
		printf("删除成功!\n\n");
		break;
	}
	default:
		break;
	}
}
//查找
void  find(pro* ps)
{
	assert(ps);
	printf("1.编号查找\n");   
	printf("2.名字查找\n");
	int op;
	printf("请选择>\n");
	scanf("%d", &op);
	int pos;
	switch (op)
	{
	case 2: {
		printf("请输入要查找的名字>\n");
		pos = find_name(ps);
		if (pos < 0) {
			printf("没有该商品的信息>\n\n");
			break;
		}
		//printf("该商品信息为>\n");
		printf("********商品信息*********\n编号    名字    进价    售价    进货量   销售量\n"); 
		printf("%s   %s   %lf   %lf   %d   %d\n", ps->arr[pos].id, ps->arr[pos].name  
			, ps->arr[pos].price_in, ps->arr[pos].price_out, ps->arr[pos].count_in, ps->arr[pos].count_out);  
		break;
	}
	case 1: {
		printf("请输入要查找的编号>\n");
		pos = find_id(ps);
		if (pos < 0) { 
			printf("没有该商品的信息>\n\n"); 
			break;
		}
		//printf("该商品信息为>\n");
		 printf("********商品信息*********\n编号    名字    进价    售价    进货量   销售量\n");
		printf("%s   %s   %lf   %lf   %d   %d\n", ps->arr[pos].id, ps->arr[pos].name 
			, ps->arr[pos].price_in, ps->arr[pos].price_out, ps->arr[pos].count_in, ps->arr[pos].count_out); 
		break;
	}
	default :
		break;
	}
}
//修改
void modtify(pro* ps)
{
	printf("1.通过编号修改\n");
	printf("2.通过名字修改\n");
	int op;
	printf("请选择>\n");
	scanf("%d", &op);
	switch (op)
	{
	case 2: {
		printf("请输入要修改商品的名字>\n");
		int pos = find_name(ps);
		if (pos < 0)
		{
			printf("没有该商品的信息>\n\n");
			break;
		}
		//修改该位置的信息
		printf("请输入商品的编号>\n");
		scanf("%s", ps->arr[pos].id);
		printf("请输入商品的名字>\n");
		scanf("%s", ps->arr[pos].name);
		printf("请输入商品的进价>\n");
		scanf("%lf", &ps->arr[pos].price_in);
		printf("请输入商品的售价>\n");
		scanf("%lf", &ps->arr[pos].price_out);
		printf("请输入商品的进货量>\n");
		scanf("%d", &ps->arr[pos].count_in);
		printf("请输入商品的售货量>\n");
		scanf("%d", &ps->arr[pos].count_out);
		printf("修改成功!\n\n");
		break;
	}
	case 1: {
		printf("请输入要修改商品的编号\n");
		int pos = find_id(ps);
		if (pos < 0)
		{
			printf("没有该商品的信息\n\n");
			break;
		}
		//修改该位置的信息
		printf("请输入商品的编号>\n"); 
		scanf("%s", ps->arr[pos].id); 
		printf("请输入商品的名字>\n"); 
		scanf("%s", ps->arr[pos].name); 
		printf("请输入商品的进价>\n"); 
		scanf("%lf", &ps->arr[pos].price_in);  
		printf("请输入商品的售价>\n"); 
		scanf("%lf", &ps->arr[pos].price_out); 
		printf("请输入商品的进货量>\n");   
		scanf("%d", &ps->arr[pos].count_in); 
		printf("请输入商品的售货量>\n"); 
		scanf("%d", &ps->arr[pos].count_out); 
		printf("修改成功!\n\n");
		break; 
	}
	default:
		break;
	}
}

//计算利润
void showfit(pro* ps)
{
	printf("********商品信息*********\n名字    利润\n");
	for (int i = 0; i < ps->size; i++)
	{
		printf("%s : %.2f\n",ps->arr[i].name ,ps->arr[i].price_out * ps->arr[i].count_out - ps->arr[i].price_in * ps->arr[i].count_in);   
	}
	printf("**************************\n\n"); 
}

//查看
void show(pro* ps)
{
	printf("********商品信息*********\n编号    名字    进价    售价    进货量   销售量\n");
	for (int i = 0; i < ps->size; i++)
		printf("%s      %s    %.2lf   %.2lf    %d     %d\n", ps->arr[i].id, ps->arr[i].name, ps->arr[i].price_in  
			, ps->arr[i].price_out, ps->arr[i].count_in, ps->arr[i].count_out);   
	printf("**************************\n\n");   
}

