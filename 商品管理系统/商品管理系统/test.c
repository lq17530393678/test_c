



#include"good.h"


void menu()
{
	printf("*****商品管理系统*****\n");
	printf("1.录入商品信息\n");
	printf("2.删除商品信息\n");
	printf("3.查找商品信息\n");
	printf("4.查看商品信息\n");
	printf("5.修改商品信息\n");
	printf("6.计算利润\n");
	printf("0.退出系统\n");
	printf("**********************\n");
}


int main()
{
	pro s;
	int op=-1;
	init(&s);
	do {
		menu(); 
		printf("请选择>\n");
		scanf("%d", &op);
		switch (op)
		{
		case 1: {printf("录入商品信息>\n");
			add(&s);
			break; }
		case 2: {printf("删除商品信息>\n");
			pop(&s);
			break; }
		case 3: {printf("查找商品信息>\n");
			find(&s);
			break;  }
		case 4: {printf("查看商品信息>\n");
			show(&s);
			break;  }
		case 5:{printf("修改商品信息>\n");
			modtify(&s);
			break;
		}
		case 6: {
			printf("计算利润\n");
			showfit(&s);
			break;
		}
		case 0://退出系统 
			printf("正在退出系统......\n");
			break;
		default:break;
		}
	} while (op);
	return 0;
}