#pragma once

#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<assert.h> 



#define max_id 20
#define max_name 20


typedef struct good_
{
	char id[max_id];//编号
	char name[max_name];//名字
	double price_in;//进价
	double price_out;//售价
	int count_in;//进货
	int count_out; //出货
}good;    


typedef struct code
{
	good* arr; 
	int capacity;
	int size; 
}pro;


//初始化
void init(pro*ps);

//销毁
void destory(pro* ps);
//扩容
void check(pro* ps);
//添加
void add(pro* ps);
//删除
void pop(pro* ps);
//指定该位置删除 
void pop_pos(pro* ps, int pos); 
//查找名字
int find_name(pro* ps);
//查找编号
int find_id(pro* ps);

//查找
void  find(pro* ps);
//修改
void modtify(pro* ps);
//查看
void show(pro* ps);
//利润
void showfit(pro* ps); 





