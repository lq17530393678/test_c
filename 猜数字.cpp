#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>


void menu()
{
	printf("*******************************\n");
	printf("******** 0.退出游戏  **********\n");
	printf("******** 1.开始游戏  **********\n");
	printf("*******************************\n");
}

void game()
{
	srand((int)time(NULL));
	int ret = rand() % 100 + 1;//产生1到100之间的随机数;
	printf("电脑已产生随机数,请输入您猜的数字>\n");
	while (1)
	{
		int guess = 0;
		scanf("%d", &guess);
		if (guess < ret)
		{
			printf("猜小了,请重新猜>\n");
		}
		else if (guess > ret)
		{
			printf("猜大了,请重新猜>\n");
		}
		else
		{
			printf("恭喜你,猜对了,这个数是%d\n", ret);
			break;
		}
	}
}
int main()
{
	int input = 0;
	do
	{
		menu();
		printf("请选择>\n");
		scanf("%d", &input);
		switch (input)
		{
		case 0:printf("退出游戏\n");
			break;
		case 1:printf("游戏开始\n");
			game();
			break;
		default:printf("输入错误,请重新输入\n");
		}
	} while (input);
}
