#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void print(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
		printf("%d ", arr[i]);
	printf("\n");
}
int main()
{
	int n = 0;
	int arr[10] = { 2,4,3,1,5,6,8,9,7,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	print(arr,sz);
	printf("请输入要查找的数\n");
	scanf("%d", &n);
	int left = 0, right = 9;
	int mid = 0;
	int flag = 0;
	while (left <= right)
	{
		mid = (right + left) / 2;
		if (arr[mid] < n)
		{
			left = mid + 1;
		}
		else if (arr[mid] > n)
		{
			right = mid - 1;
		}
		else if (arr[mid] == n)
		{
			flag = 1;
			break;
		}
	}
	if (flag == 0)
	{
		printf("没有找到\n");
	}
	else
		printf("找到了,这个数是下标是%d", mid);
	return 0;
}
