#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

void printOddEvenBits(int num) {
    int i;
    int num_bits = sizeof(int) * 8; // 获取整数的位数

    printf("Binary representation of %d: ", num);

    // 打印奇数位
    printf("Odd bits: ");
    for (i = num_bits - 1; i >= 0; i -= 2) {
        printf("%d", (num >> i) & 1);
    }
    printf("\n");

    // 打印偶数位
    printf("Even bits: ");
    for (i = num_bits - 2; i >= 0; i -= 2) {
        printf("%d", (num >> i) & 1);
    }
    printf("\n");
}

int main() {
    int num = 42; // 例如，要打印42的奇数位和偶数位

    printOddEvenBits(num);

    return 0;
}
