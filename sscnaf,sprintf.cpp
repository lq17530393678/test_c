#include<stdio.h>
struct S
{
	char name[50];
	int n;
	float m;
};//定义一个结构体 
int main()
{
	struct S k={"zhangsan",60,66.6};
	struct S tmp;
	char arr[50];
	sprintf(arr,"%s,%d,%.2f",k.name,k.n,k.m);//将输入的内容打入数组中; 
	sscanf(arr,"%s,%d,%f",tmp.name,tmp.n,tmp.m);//从数组中按照对应的格式读取到相应的变量中; 
	printf("%s,%d,%.2f",tmp.name,tmp.n,tmp.m);//将变量的内容打印到屏幕上; 
}
