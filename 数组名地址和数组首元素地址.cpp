//数组名地址和取地址名地址;
#include<stdio.h>
int main() {
	int arr[10]={1,2,3,4,5,6,7,8,9,10};
	printf("取数组地址 %p %p\n",&arr+1,&arr);//整个数组 
	printf("取数组首地址 %p\n",&arr[0]);
	printf("数组 %p\n",arr+1);
}
//1.当sizeof(arr)时，为表示整个数组；
//2.当&arr时，为整个数组的地址; 
