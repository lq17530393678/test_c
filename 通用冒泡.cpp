#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

void int_bulle(int arr[], int sz)
{
	for (int i = 0; i < sz; i++)
	{
		for (int j = 0; j < sz - i; j++)
		{
			int t = 0;
			if (arr[j] > arr[j + 1])
			{
				t = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = t;
			}
		}
	}
}

int main()
{
	int arr[10] = { 1,2,5,4,6,3,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	int_bulle(arr, sz);
}
