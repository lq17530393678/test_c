#include <stdio.h>
int strcompare(char *s1, char *s2)
{
	while(*s1!='\0')
	{
		*s1++;
		*s2++;
		if(*s1>*s2)
		{
			return 1;
		}
		else if(*s1<*s2)
		{
			return -1;
		}
	}
	if(*s1==*s2)
	return 0;
}
int main() 
{
	char *s1 = "ABCD";
	char *s2 = "ABCD";
	printf("%d\n", strcompare(s1, s2));
	return 0;
}
