#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
//按位操作符
//  <<  >>  &  ^  |  ~  6个
//只有~是单目运算符
//求的二进制数的1是补码中的
int mount(unsigned int num)
{
	int count = 0;
	while (num)
	{
		if (num % 2 == 1)
		{
			count++;
		}
		num /= 2;
	}
	return count;
}
int main()
{
	int num = 0;
	scanf("%d", &num);
	int count = mount(num);
	printf("%d", count);
}
