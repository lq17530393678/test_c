#include<stdio.h>
void reverse(int *p,int n)
{
	for(int i=0;i<n/2;i++)
	{
		int t;
		t=*(p+i);
		*(p+i)=*(p+n-i-1);
		*(p+n-i-1)=t;
	}
}
int main()
{
	int arr[4]={10,20,30,40};
	int i;
	reverse(arr,5);
	for(i=0;i<5;i++)
	{
		printf("%d\n",arr[i]);
	}
	return 0;
}
