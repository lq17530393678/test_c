#include<stdio.h>
int main()
{
	int arr[5]={0,1,2,3,4};
	int (*p)[5]=&arr;
	for(int i=0;i<5;i++)
	{
		printf("%d ",*(*p+i)); 
	}
 } 
 /*p取整个元素的地址，此时*p不表示整个数组的值,而是数组
 的首元素地址,相当于指向，所以需要再次解引用*/ 
