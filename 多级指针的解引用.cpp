#include<stdio.h>
int main()
{
	char str[]="abcd";
	char *t=str;
	char *arr[]={t,"efgh","mine"};
	char **p=arr;//二级指针指向一级指针数组,合情合理; 
	printf("%s\n",*p);//p是t的地址,*p是数组的地址;输出字符串可以直接用首地址; 
	printf("%s\n",t);//t就是字符数组的首地址.相当于是数组名; 
	printf("%s",str);
	//当遇到多级指针的时候,就要画图,分析谁是谁的地址; 
}
