#include<stdio.h>
int sum(int x, int y)
{
	return x + y;
}
int jian(int x, int y)
{
	return x - y;
}
int main()
{
	//函数指针就是指向函数的指针 ,格式为:(指针)(形参类型，形参类型);
	printf("函数指针就是指向函数的指针 ,格式为:指针(形参类型，形参类型);\n");
	int (*p1)(int, int) = sum;//函数指针指向的一个函数；此时*p,p,sum的意义等同；
	int (*p2)(int, int) = jian;
	int (*p[2])(int, int) = { p1,p2 };//这里用指针数组进行储存两个函数指针，所以函数指针数组就是储存函数指针的指针数组;
	printf("使用函数指针的三种形式\n");
	printf("使用函数指针数组进行解引用:%d\n", (*p[0])(4, 2));//使用第一个函数指针；
	printf("直接使用指针变量:%d\n", (p1)(4, 2));
	printf("使用函数指针进行解引用:%d\n",(*p1)(4, 2));
	printf("%d", (*p[1])(4, 2));//
}

