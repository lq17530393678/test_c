#include <stdio.h>
#define M 10
struct student{
	long num;
	double score;
};
int fun(struct student *s,struct student *b,int high,int low)
{
	int k=0;
	struct student *p=s,*q=b;
	for(;p<s+M;p++)
	if(p->score>=low && p->score<=high)
	{
		*q++=*p;
		k++;
	}	
	return k;
} 
int main()
{
	int k,low,high;
	struct student s[M];
	struct student b[M];
	struct student *x=s;
	struct student *y=b;
	for(;x<s+M;x++)
	scanf("%ld%lf",&x->num,&x->score);
	scanf("%d%d",&low,&high);
	k=fun(s,b,high,low);
	for(;y<b+k;y++)
	printf("%ld %lf\n",y->num,y->score);
	return 0;
}
