#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#define int long long
signed my_int(const void* p1, const void* p2)
{
    return *(int*)p1 - *(int*)p2;
}

signed main()
{
    int arr[200000];
    int n = 0;
    scanf("%lld", &n);
    for (int i = 0; i < n; i++)
        scanf("%lld", &arr[i]);
    qsort(arr, n, sizeof(int), my_int);
    int count = 0;
    int pre = arr[0];
    for (int i = 0; i < n; i++)
    {
        if (arr[i] == pre)
        {
            count++;
        }
        else
        {
            printf("%lld %lld\n", pre, count);
            pre = arr[i];
            count = 1;
        }
    }
    printf("%d %d",pre,count);
    return 0;
}
