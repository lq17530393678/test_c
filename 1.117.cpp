#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
void init(int arr[],int sz)
{
	memset(arr, 0, sz * sizeof(arr[0]));
}
void print(int arr[],int sz)
{
	for (int i = 0; i < sz; i++)
		printf("%d ", arr[i]);
	printf("\n");
}
void reverse(int arr[],int sz)
{
	int left = 0;
	int right = sz - 1;
	while (left <= right)
	{
		int t = 0;
		t = arr[left];
		arr[left] = arr[right];
		arr[right] = t;
		left++, right--;
	}
}
int main()
{
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	reverse(arr, sz);
	print(arr, sz);
	init(arr, sz);
	print(arr, sz);
}
