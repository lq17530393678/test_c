#include<stdio.h>
void swap(int *x,int *y)
{
	int t;
	t=*x;
	*x=*y;
	*y=t;
}
int main()
{
	int a,b;
	scanf("%d%d",&a,&b);
	void (*p)(int *x ,int *y)=swap;
	(*p)(&a,&b);
	printf("%d %d",a,b);
}
