#include<stdio.h>
int main()
{
	 FILE *pf=fopen("测试.txt","r");//此处打开文件用fopen; 
	 if(pf==NULL)//判断指针是否为空; 
	 {
	 	perror("fopen");
	 	return 1;
	 }
	int ret=fgetc(pf);//注意的是返回值是整形;进行读取文件指针的一个字符 
	printf("%c\n",ret);//进行打印一个字符,打印的值ASCLL码值; 
	ret=fgetc(pf);//再次读取的就是下一个字符; 
	printf("%c\n",ret);//再次打印; 
	ret=fgetc(pf);//再次读取下一个字符; 
	printf("%c",ret);//再次打印; 
	return 0;
}
