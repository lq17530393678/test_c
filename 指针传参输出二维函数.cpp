//指针传参输出二维函数

#include<stdio.h>
void shuchu(int (*p)[5],int x,int y)
{
	for(int i=0;i<x;i++)
	{
		for(int j=0;j<y;j++)
		{
			printf("%d ",*(*(p+i)+j));
		}
		printf("\n");
	}
}
int main()
{
	int a=3,b=5;
	int arr[3][5]={{1,2,3,4,5},{6,7,8,9,10},{11,12,13,14,15}};
	shuchu(arr,a,b);
}
