#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
struct data
{
	int n;
	int ptr[0];//ptr[]或者ptr[0];
};//柔性数组
int main()
{
	struct data* p = (struct data*)malloc(sizeof(int)+10*sizeof(int));
	p->n = 10;
	for (int i = 0; i < 10; i++)
	{
		*(p->ptr + i) = i;
	}
	for (int i = 0; i < 10; i++)
		printf("%d ", *(p->ptr+i));//不可以是p->ptr++，因为是函数名
	return 0;
}
