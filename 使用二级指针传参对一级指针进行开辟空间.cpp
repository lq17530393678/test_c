#include<stdio.h>
#include<stdlib.h>
#include<string.h>
void fun(char **p)//二级指针接收str的地址; 
{
	*p=(char*)malloc(100);//开辟堆区的空间传给一级指针str;
	//此时一级指针指向这块空间; 
}
int main()
{
	char *str=NULL;
	fun(&str);//传回来的是指向堆区的指针; 
	strcpy(str,"hello world");//此时进行拷贝； 
    printf(str);//printf("hello world")中其实就是接收到字符串的地址,然后进行打印, 
	free(str);//所以可以直接输入字符串名字,进行打印； 
	str=NULL;
	return 0;
 } 
