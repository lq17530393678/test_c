
#define _CRT_SECURE_NO_WARNINGS 1  
#include<stdio.h>
typedef struct singer
{
	double grade;
	int id;
}si;
void swap(si* a, si* b)
{
	si temp;
	temp = *a;
	*a = *b;
	*b = temp;
}
int min(int a, int b)
{
	return a > b ? b : a;
}
int max(int a, int b)
{
	return a > b ? a : b;
}
void sort(si arr[], int n)
{
	int i, j;
	for (i = 1; i <= n-1; i++)
		for (j = 1; j <= n - i; j++)
			if (arr[j].grade < arr[j + 1].grade)
				swap(&arr[j], &arr[j + 1]);
}
void print(si arr[], int n)
{
	int i;
	printf("output:\nscores:\n");
	for (i = 1; i <= n; i++)
		printf("NO.%d:%.2f\n", arr[i].id, arr[i].grade);

}
int main()
{
	int a = 10, i;
	si arr[11];
	printf("Please input singer's score;\n");
	for (i = 1; i <= a; i++)
	{
		int num;
		int max1 = 0;
		int min1 = 10;
		int sum = 0;
		int j;
		for (j = 1; j <= 6; j++)
		{
			scanf("%d", &num);
			max1 = max(max1, num);
			min1 = min(min1, num);
			sum = sum + num;
		}
		sum = sum - max1;
		sum = sum - min1;
		arr[i].grade = 1.0 * sum / 4;
		arr[i].id = i;
	}
	sort(arr, a);
	print(arr, a);

	return 0;
}
