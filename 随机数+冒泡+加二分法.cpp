#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
int main()
{
	srand((int)time(NULL));
	int arr[10];
	for (int i = 0; i < 10; i++)
	{
		arr[i] = rand() % 100 + 1;
	}
	int t;
	for (int i = 0; i < 9; i++)
	{
		for (int j = 0; j < 9 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				t = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = t;
			}
		}
	}//先对随机数进行排序
	printf("随机数为:\n");
	for (int k = 0; k < 10; k++)
	{
		printf("%d ", arr[k]);
	}
	printf("\n");
	int flag, mid;
	int left = 0, right = 9;
	printf("请输入目标数:\n");
	scanf("%d", &flag);
	while (left <= right)
	{
		mid = (left + right) / 2;
		if (arr[mid] < flag)
		{
			left = mid + 1;
		}
		if (arr[mid] > flag)
		{
			right = mid - 1;
		}
		if (arr[mid] == flag)
		{
			printf("%d", mid + 1);
			break;
		}
		if (left == right &&left==mid&& arr[mid] != flag)
		{
			printf("can't find %d", flag);
			break;
		}
	}
	return 0;
}

