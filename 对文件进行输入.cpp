#include<stdio.h>
int main()
{
	FILE *pf=fopen("测试.txt","w");//打开目标文件进行写; 
	if(pf==NULL)//此处如果打开失败就报错； 
	{
		perror("fopen");//perror是报错函数; 
		return 1;
	}
	fputc('b',pf);//fputc是将一个字符写进文件指针里; 
	fputc('i',pf);
	fputc('t',pf);
	fclose(pf);//对文件指针进行完操作后要关闭文件指针; 
	pf=NULL;//指针为空; 
	return 0; 
}
