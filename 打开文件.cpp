#include<stdio.h>
int main()
{
	FILE *pf=fopen("text.dat","r");//定义FILE类型的指针pf,打开了一个不存在的文件; 
	if(pf==NULL)
	{
		perror("fopen");//perror是对文件操作的报错说明; 
		return 1;
	}
	fclose(pf);//最后对文件进行关闭,否则就会打不开文件. 
	return 0;
}
