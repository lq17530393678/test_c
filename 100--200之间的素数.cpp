#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int is_prime(int n)
{
	for (int i = 2; i < n; i++)
	{
		if (n % i == 0)
		{
			return 0;
		}
	}
	return 1;
}
int main()
{
	printf("100--200之间的素数是\n");
	for (int i = 100; i <= 200; i++)
	{
		if (is_prime(i) == 1)
		{
			printf("%d ", i);
		}
	}
	return 0;
}
